execute pathogen#infect()

syntax enable
set gfn=Source\ Code\ Pro:h14

set undodir=~/.vim/undodir
set undofile

set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2 

set number
set ruler
set hlsearch
 
filetype plugin indent on
set termguicolors

cmap w!! w !sudo tee > /dev/null %
set backspace=indent,eol,start

map <Tab> :tabn<cr>
map <S-Tab> :tabp<cr>

set clipboard=unnamed

set background=dark
set t_ut=
colorscheme gruvbox
